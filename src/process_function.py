import logging
import pandas as pd

def croisement_list_df(liste,df,colonne,new_col)-> pd.DataFrame:
    df_union = df.head(0)                                   ### générer un df vide pour garder la structure
    for c in liste:                                         ### boucler sur la liste des medicament et empilé le resultat en ajoutant une nouvelle colonne 'new_col' qui contient le nom du médicament trouvé dans le journal
        df_tmp = df[df[colonne].str.contains(c)]            ### verifier si le nom du medicament ce trouve dans la colonne titre
        df_tmp.insert(0,new_col,c)                          ### ajout de la colonne drug dans le df_tmp
        df_union = union_table(df_union,df_tmp)             ### empiler (faire la boucle jusqu'a avoir checker tout les noms de médicaments)


def union_table(df1,df2):
    df_u = pd.concat([df1,df2])         ######### union
    return df_u
   

def read_file_csv(path):                ######## Lire avec un check si c'est bien un csv
    if path[-3:]=='csv':
        df = pd.read_csv(path)
        return df;
    else:
        logging.error('erreur le fichier '+path+' doit être un csv')      
    return 0;
    
def read_file_json(path):
    if path[-4:]=='json':               ######## Lire avec un check si c'est bien un json
        df = pd.read_json(path)
        return df;
    else:
        logging.error('erreur le fichier '+path+' doit être un json')   
    return 0;
    
