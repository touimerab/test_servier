import launcher
import process_function
import configparser


def main():
    config = configparser.ConfigParser()  ### On récupére le fichier de config ce trouvant dans parm on le parse, et on lance le launcher
    config.read('../parm/chemin_fichier.conf')
    launcher.launcher(config)                        

if __name__ == "__main__":
    data = "Debut de l'éxecution"
    print(data)
    main()
    