import os
import pandas as pd
import json
import process_function


def launcher(conf):
    ########## Lire les fichier récupéré depuis la config et initialisé 1 df pour chaque fichier
    df_drug = process_function.read_file_csv(conf['CHEMIN_CSV']['drug'])
    df_pubmed_csv = process_function.read_file_csv(conf['CHEMIN_CSV']['pubmed'])
    df_clinical_trials = process_function.read_file_csv(conf['CHEMIN_CSV']['clinical_trials'])
    df_pubmed_json = process_function.read_file_json(conf['CHEMIN_JSON']['pubmed'])
    
    
    ########## Transformer les colonnes dates des df vers une colonne date iso 
    df_pubmed_json['date'] = pd.to_datetime(df_pubmed_json['date'], dayfirst=True)
    df_pubmed_csv['date'] = pd.to_datetime(df_pubmed_csv['date'], dayfirst=True)
    df_clinical_trials['date'] = pd.to_datetime(df_clinical_trials['date'], dayfirst=True)
    
    ########## faire une union entre les df pubmed reçu du csv et du json (les colonnes sont les mêmes) avec process_function.union_table()
    df_pubmed = process_function.union_table(df_pubmed_json,df_pubmed_csv)
    
    ########## Récupérer le nom des médicaments dans une liste col_nom_drug
    col_nom_drug = df_drug['drug'].tolist()
    
    ######## Croiser col_nom_drug et vérifier si elle est contenu dans les 'title' de df_pubmed et generer la nouvelle colonne drug

    df_union_pubmed = df_pubmed.head(0)                                                    ### générer un df vide pour garder la structure
    for c in col_nom_drug:                                                                 ### boucler sur la liste des medicament et empilé le resultat en ajoutant une nouvelle colonne 'new_col' qui contient le nom du médicament trouvé dans le journal
        df_tmp_pm = df_pubmed[df_pubmed['title'].str.contains(c)]                          ### verifier si le nom du medicament ce trouve dans la colonne titre
        df_tmp_pm.insert(0,'drug',c)                                                       ### ajout de la colonne drug dans le df_tmp
        df_union_pubmed = process_function.union_table(df_union_pubmed,df_tmp_pm)                            ### empiler (faire la boucle jusqu'a avoir checker tout les noms de médicaments)

    
    
    
    ######## Croiser avec le df des essaie clinique avec la colonne scientific_title et generer la nouvelle colonne drug
   
    df_union_cl = df_clinical_trials.head(0)                                                   ### générer un df vide pour garder la structure
    for c in col_nom_drug:                                                                            ### boucler sur la liste des medicament et empilé le resultat en ajoutant une nouvelle colonne 'new_col' qui contient le nom du médicament trouvé dans le journal
        df_tmp_cl = df_clinical_trials[df_clinical_trials['scientific_title'].str.contains(c)]            ### verifier si le nom du medicament ce trouve dans la colonne titre
        df_tmp_cl.insert(0,'drug',c)                                                                ### ajout de la colonne drug dans le df_tmp
        df_union_cl = process_function.union_table(df_union_cl,df_tmp_cl)                                          ### empiler (faire la boucle jusqu'a avoir checker tout les noms de médicaments)
  
 
    ###### grouper les deux df dans un nouveau df avec les colonnes 'date','journal','drug','publication'
    df_clinical_trials['publication']=df_clinical_trials['scientific_title']
    df_clinical_trials = df_clinical_trials.drop(columns=['id', 'scientific_title'])
    df_union_pubmed['publication']=df_union_pubmed['title']
    df_union_pubmed=df_union_pubmed.drop(columns=['id', 'title'])
    df_final = process_function.union_table(df_clinical_trials,df_union_pubmed)
    
    ###### indexer le df par drug/journal, pour avoir une visibilité par couple (nom_medicament,journal)->(publication,date) 
    ###### comme ça on a quel journal a publié quel medicament, et pour chaque couple on a une liaison 1..n où par la suite si on a des nouveau articles on rajoute seulement à la clé 
    ###### Pour le graph de liaison j'ai choisis comme clé (drug,journal) et la valeur associé sera les (nom de publication, date) 
    df_final.set_index(['drug','journal'], inplace = True) ## On met le nouveau index 
    df_result = df_final.groupby(['drug', 'journal'], group_keys=True).apply(lambda x: x.to_json(orient='records'))## on groupe by puis en enregistre le nouveau format json en sortie
    print("le fichier json en resultat a bien été géréné dans le repos ../parm/result")
    df_result.to_json("../output/result.json")
    ######## à mettre en commentaire pour activer ou desactivé
    fonction_annexe(df_result)
    
    
    
def fonction_annexe(df):
    ########## Question Annexe: la lecture d'un fichier json orienté records peut être compliqué
    ########## Pour repondre a l'annexe, on peux directement récupéré le df en fin de traitement, desactiver l'index ('drug','journal') qu'on a fait avant, et crée un nouvelle index avec un group by count et selectionner le max 
    ########## ou sinon on récupére juste les indexs et on cherche lle journal avec la plus grande fréquence d'apparition
    df = df.to_frame().reset_index()
    print("Le journal ayant le plus de médicament récupérer depuis le fichier drug est :")
    print(df.groupby('journal')['drug'].count().reset_index(name="count").max()) ##### On groupe by le nom du journal par medicament et on selectionne le max, avec un print ou en le mettant dans un fichier
    