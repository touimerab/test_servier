# test_servier



## Getting started

1.
J'ai organiser le projet python avec plusieurs sous dossier :
- bin : pour le package a générer
- test: pour les notesbooks de test
- script: pour les scripts si besoin
- file:pour les fichier en entrée
- src : contenant le code source
- shell: pour les batch a éxécuter si besoin
- output: contenant le json en sortie 
- tmp : pour les fichiers temporaire
- parm : pour les fichier de parametrage (dans notre cas contenant les chemins vers les fichiers dans file en entrée


Pour tester cloner le projet, puis executer src/main.py
- Vous pouvez tester avec d'autre fichier : en remplaçant les fichiers en entrée ce trouvant dans /file et en mentionnant le nom du fichier dans /parm/chemin.conf (note on peux avoir
- un autre nom de fichier juste le rajouter dans conf)
- Le resultat sera dans le fichier /output/result.json
- Annexe: vous pouvez observer le resultat de la feature demandé dans annexe sur la console python un print est mis a disposition


2.
Séparations : dans notre cas nous avons rapporter le brut directement dans des df pandas pour pouvoir les traiter par la suite;
(le seul fichier écris en resultat du job est le json en output)

- Le code est structure dans les fichiers :
	 - main : étant le point d'entree de l'application, on parse le fichier de config récupéré depuis /parm et en l'envoie au launcher
	 - launcher : fait le traitement en batch et les fonctionnalités spécifique a notre cas d'usage
	 - process_function : les differentes fonctions utilisé dans le launcher, les fonctions sont réutilisables.

- Les fonctions peuvent étre intégrés dans le DAG
- Et j'ai garder le répertoire bin pour la génération des differents package.


3.Data pipeline

dans le code la structure est la suivante :
- récupération de la données et mis a disposition dans les df 
- netoyage et prétraitement de la données dans tout les df
- concaténation des données pubmed et regroupement des sources dans le même df
- récupération des noms de médicaments depuis la table drug dans une liste
- croisement et vérification de la contenance des noms dans les 2 sources
- rajout de colonne si le médicament existe mentionnant le nom du médicament
- Union des deux df des 2 sources
- grouper by par medicament et par journal
- écruture du json avec la 	"clé" (medicament/journal): "valeur" (date+titre)
- +annexe
	
	
4.Traitement ad-hoc (annexe):
	 - ajout de la fonction annexe
    Pour extraire depuis le json la solution choisis est la récupération du dernier df enlevé l'ancienne indexation puis faire le count group by journal partition by medicament qui récupére le nombre de médicament par journal; 	groupby(journal)[drug]
	
	
6.Pour aller plus loin
	On peux utiliser Spark dans le cas de plusieurs TO de données;
	migration de la library pandas vers le framework spark à installé sur Dataproc, pour le stockage en peux utiliser le même GCS
	dans le cas de fichiers de plusieurs TO on peut êssayer le même code, mais pour des millions de fichiers, l'utilisation est plus complexe
	
	
Requête SQL:

1.1ere requête

select date,sum(prod_price*prod_qty) as somme_des_ventes_par_jour 
from TRANSACTION 
where date between '01/01/2019' and '31/12/2019' 
group by date; 
---------------------

2eme requête 

select t.client_id,count(case product_type when "MEUBLE" then 1 else null end) as ventes_meuble ,count(case product_type when "DECO" then 1 else null end) as ventes_deco
from TRANSACTION t
inner join PRODUCT_NOMENCLATURE p ON t.prop_id = p.product_id 
where t.date between '01/01/2019' and '31/12/2019' 
group by t.client_id


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/touimerab/test_servier.git
git branch -M main
git push -uf origin main
```


For open source projects, say how it is licensed.

